package com.saklayen.common.ui.util

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide


@BindingAdapter("loadImage")
fun ImageView.loadImage(url: String?) {
    Glide.with(context)
        .load(url)
        .placeholder(drawable)
        .into(this)
}

@BindingAdapter("goneUnLess")
fun View.goneUnless(condition: Boolean) {
    if (condition.not()) this.visibility = View.GONE
    else this.visibility = View.VISIBLE
}


@BindingAdapter("invisibleIf")
fun View.invisibleIf(condition: Boolean) {
    if (condition) this.visibility = View.INVISIBLE
    else this.visibility = View.VISIBLE
}
