plugins {
    id("codechallenge.android.library")
    id("codechallenge.android.hilt")
    id("kotlin-kapt")
}


android {
    namespace = "com.saklayen.common.ui"

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }

}

dependencies {
    implementation(projects.common.base)

    api(libs.androidx.core.ktx)
    api(libs.androidx.appcompat)
    api(libs.material)
    api(libs.androidx.activity)
    api(libs.androidx.constraintlayout)
    api(libs.androidx.splashscreen)
    api(libs.androidx.lifecycle.viewmodel.ktx)
    api(libs.androidx.navigation)
    api(libs.androidx.navigation.ui)
    api(libs.glide)
    api(libs.glide.compiler)

    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)


}