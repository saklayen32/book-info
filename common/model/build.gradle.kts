plugins {
    id("codechallenge.android.library")
    id("codechallenge.android.moshi")
}

android {
    namespace = "com.saklayen.common.model"

}

dependencies {
    implementation(projects.common.base)

}