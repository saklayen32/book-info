package com.saklayen.common.base.analytics

import android.util.Log
import timber.log.Timber

class CrashReportingTree(private val log: (priority: Int, tag: String?, message: String, t: Throwable?) -> Unit) :
    Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }
        log.invoke(priority, tag, message, t)
        TODO("Saklayen add Crashlytics")
//        Crashlytics.log(mPriority, mTag, mMessage)
//        if (mThrowable != null) {
//            Crashlytics.logException(mThrowable)
//        }
    }
}
