package com.saklayen.common.base.domain.result

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    NOTHING
}