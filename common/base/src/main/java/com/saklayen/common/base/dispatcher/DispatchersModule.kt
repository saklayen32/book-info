package com.saklayen.common.base.dispatcher

import com.saklayen.common.base.dispatcher.AppDispatchers.DEFAULT
import com.saklayen.common.base.dispatcher.AppDispatchers.IO
import com.saklayen.common.base.dispatcher.AppDispatchers.MAIN
import com.saklayen.common.base.dispatcher.AppDispatchers.MAIN_IMMEDIATE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * @Dispatcher(IO) ioDispatcher: CoroutineDispatcher
 */

@Module
@InstallIn(SingletonComponent::class)
object DispatchersModule {

    @Provides
    @Dispatcher(DEFAULT)
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @Provides
    @Dispatcher(IO)
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @Dispatcher(MAIN)
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main

    @Provides
    @Dispatcher(MAIN_IMMEDIATE)
    fun providesMainImmediateDispatcher(): CoroutineDispatcher = Dispatchers.Main.immediate
}
