plugins {
    id("codechallenge.android.library")
    id("codechallenge.android.hilt")
    id("codechallenge.android.moshi")
}


android {
    namespace = "com.saklayen.common.base"

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }

}

dependencies {
    api(libs.material)
    api(libs.kotlinx.coroutines.android)
    api(libs.timber)
}
