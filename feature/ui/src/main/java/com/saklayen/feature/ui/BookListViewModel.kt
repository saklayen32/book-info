package com.saklayen.feature.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.saklayen.core.domain.usecase.BookListApiUseCase
import com.saklayen.core.model.Book
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class BookListViewModel @Inject constructor(private val bookListApiUseCase: BookListApiUseCase): ViewModel() {

    private val _navigate = Channel<NavigationAction> (Channel.CONFLATED)
    val navigate = _navigate.receiveAsFlow()

    private val _fetchBookList = MutableSharedFlow<Unit>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )
    val bookListApiResponse = _fetchBookList.flatMapLatest {
        bookListApiUseCase(Unit)
    }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(5_000), null)

    init {

        viewModelScope.launch {
            fetchBookList()
        }

        bookListApiResponse.onEach {
            Timber.d("response = $it")

        }.launchIn(viewModelScope)
    }

    private fun fetchBookList() {
        _fetchBookList.tryEmit(Unit)
    }

    fun selectBook(book: Book) {
        _navigate.trySend(NavigationAction.NavigateToBookDetails(book))
    }


}

sealed interface NavigationAction {
    data class NavigateToBookDetails(val book: Book): NavigationAction
}
