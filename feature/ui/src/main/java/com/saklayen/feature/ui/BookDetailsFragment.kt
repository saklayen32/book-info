package com.saklayen.feature.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.saklayen.common.ui.BaseFragment
import com.saklayen.common.ui.util.launchAndRepeatWithViewLifecycle
import com.saklayen.feature.users.R
import com.saklayen.feature.users.databinding.FragmentBookDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookDetailsFragment :
    BaseFragment<FragmentBookDetailsBinding>(R.layout.fragment_book_details) {

    private val viewModel: BookListViewModel by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel

        launchAndRepeatWithViewLifecycle {


        }
    }
}
