package com.saklayen.feature.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.saklayen.core.model.Book
import com.saklayen.feature.users.databinding.ItemBookListBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class BooksAdapter(private val viewModel: BookListViewModel) :
    ListAdapter<Book, BooksAdapter.StringViewHolder>(DiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder {
        return StringViewHolder(
            ItemBookListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        holder.onBind(getItem(position), viewModel)
    }

    class StringViewHolder(private val binding: ItemBookListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(data: Book, viewModel: BookListViewModel) {
            binding.item = data
            binding.viewModel = viewModel
        }
    }

    private class DiffCallBack : DiffUtil.ItemCallback<Book>() {
        override fun areItemsTheSame(
            oldItem: Book,
            newItem: Book
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: Book,
            newItem: Book
        ): Boolean =
            oldItem == newItem
    }
}

@ExperimentalCoroutinesApi
@BindingAdapter(value = ["bindBookList", "bindBookListViewModel"], requireAll = true)
fun RecyclerView.bindBookList(
    data: List<Book>?,
    viewModel: BookListViewModel
) {

    if (adapter == null) adapter = BooksAdapter(viewModel)
    val value = data ?: emptyList()
    val booksAdapter = adapter as? BooksAdapter
    booksAdapter?.submitList(value)
}
