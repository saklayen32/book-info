package com.saklayen.feature.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.saklayen.common.ui.BaseFragment
import com.saklayen.common.ui.util.launchAndRepeatWithViewLifecycle
import com.saklayen.common.ui.util.navigate
import com.saklayen.feature.users.R
import com.saklayen.feature.users.databinding.FragmentBookListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class BookListFragment : BaseFragment<FragmentBookListBinding>(R.layout.fragment_book_list) {

    private val viewModel: BookListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel

        launchAndRepeatWithViewLifecycle {

            launch {
                viewModel.navigate.collect {
                    when(it) {
                        is NavigationAction.NavigateToBookDetails -> navigate(BookListFragmentDirections.navigateToBookDetailsFragment())
                    }
                }
            }

        }
    }
}
