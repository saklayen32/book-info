plugins {
    id("codechallenge.android.library")
    id("codechallenge.android.hilt")
}

android {
    namespace = "com.saklayen.core.data"

}


dependencies {
    implementation(projects.common.base)
    implementation(projects.core.domain)
    implementation(projects.core.model)
    implementation(projects.core.network)

}