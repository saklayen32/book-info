package com.saklayen.core.data.module

import com.saklayen.core.data.BookListRepositoryImpl
import com.saklayen.core.domain.BookListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal object RepositoryModule {

    @Provides
    fun provideBookListRepository(
        repository: BookListRepositoryImpl
    ): BookListRepository = repository
}
