package com.saklayen.core.data

import com.saklayen.common.base.dispatcher.AppDispatchers
import com.saklayen.common.base.dispatcher.Dispatcher
import com.saklayen.common.base.domain.result.Result
import com.saklayen.common.base.utils.ControlledRunner
import com.saklayen.core.domain.BookListRepository
import com.saklayen.core.model.BookList
import com.saklayen.core.network.ApiService
import com.saklayen.core.network.NetworkResource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookListRepositoryImpl @Inject constructor(
    @Dispatcher(AppDispatchers.IO) val dispatcher: CoroutineDispatcher,
    val apiService: ApiService
) : BookListRepository {

    private val controlledRunner = ControlledRunner<Flow<Result<BookList>>>()
    override suspend fun fetchBookList(): Flow<Result<BookList>> {
        return controlledRunner.joinPreviousOrRun {
            object : NetworkResource<BookList>(dispatcher) {
                override suspend fun createCall() = apiService.fetchBookList()
            }.asFlow()
        }

    }
}
