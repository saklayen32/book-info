plugins {
    id("codechallenge.android.library")
    id("codechallenge.android.hilt")
    id("codechallenge.android.moshi")
    alias(libs.plugins.jetbrainsKotlinAndroid)
}

android {
    namespace = "com.saklayen.core.network"

    buildFeatures {
        buildConfig = true
    }

}

dependencies {
    implementation(projects.common.base)
    implementation(projects.core.model)
    implementation(libs.retrofit.converter.moshi)
    implementation(libs.retrofit.core)
    implementation(libs.okhttp.core)

}
