package com.saklayen.core.network

import com.saklayen.core.model.BookList
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface ApiService {

    @GET("book-list")
    fun fetchBookList(): Flow<ApiResponse<BookList>>
}
