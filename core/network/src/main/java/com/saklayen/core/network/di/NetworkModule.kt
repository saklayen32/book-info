package com.saklayen.core.network.di

import android.content.Context
import com.saklayen.core.network.ApiService
import com.saklayen.core.network.BuildConfig
import com.saklayen.core.network.adapter.FlowCallAdapterFactory
import com.saklayen.core.network.interceptor.MockServerInterceptor
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object NetworkModule {

    @Singleton
    @Provides
    fun provideMockServerInterceptor(@ApplicationContext context: Context): Interceptor {
        return MockServerInterceptor(context)
    }
    @Singleton
    @Provides
    fun provideOkHttpClient(
        @ApplicationContext context: Context,
        mockServerInterceptor: Interceptor
    ) =
        OkHttpClient.Builder()
            .readTimeout(300, TimeUnit.SECONDS)
            .writeTimeout(300, TimeUnit.SECONDS)
            .connectTimeout(300, TimeUnit.SECONDS)
            .followRedirects(true)
            .addInterceptor(mockServerInterceptor)
            .build()


    @ExperimentalCoroutinesApi
    @Singleton
    @Provides
    fun provideRetrofit(
        mClient: OkHttpClient,
        mMoshi: Moshi
    ): Retrofit =
        Retrofit.Builder()
            .client(mClient)
            .baseUrl(BuildConfig.SERVER_URL.toHttpUrl())
            .addConverterFactory(MoshiConverterFactory.create(mMoshi))
            .addCallAdapterFactory(FlowCallAdapterFactory())
            .build()

    @Singleton
    @Provides
    fun provideApiService(mRetrofit: Retrofit): ApiService =
        mRetrofit.create(ApiService::class.java)
}
