plugins {
    id("codechallenge.android.library")
    id("codechallenge.android.hilt")
}

android {
    namespace = "com.saklayen.core.domain"

}

dependencies {
    implementation(projects.common.base)
    implementation(projects.core.model)

}
