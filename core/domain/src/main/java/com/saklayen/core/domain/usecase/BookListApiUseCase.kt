package com.saklayen.core.domain.usecase

import com.saklayen.common.base.dispatcher.AppDispatchers
import com.saklayen.common.base.dispatcher.Dispatcher
import com.saklayen.common.base.domain.result.Result
import com.saklayen.common.base.domain.usecase.FlowUseCase
import com.saklayen.core.domain.BookListRepository
import com.saklayen.core.model.BookList
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookListApiUseCase @Inject constructor(
    @Dispatcher(AppDispatchers.IO)
    private val ioDispatcher: CoroutineDispatcher,
    private val repository: BookListRepository
) : FlowUseCase<Unit, BookList>(ioDispatcher) {
    override suspend fun execute(parameters: Unit): Flow<Result<BookList>> {
        return repository.fetchBookList()
    }
}
