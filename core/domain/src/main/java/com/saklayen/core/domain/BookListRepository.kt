package com.saklayen.core.domain

import com.saklayen.common.base.domain.result.Result
import com.saklayen.core.model.BookList
import kotlinx.coroutines.flow.Flow

interface BookListRepository {

    suspend fun fetchBookList(): Flow<Result<BookList>>
}