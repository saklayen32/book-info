package com.saklayen.core.model


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
data class BookList(
    @Json(name = "data")
    val books: List<Book>,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Int
)

@Parcelize
@JsonClass(generateAdapter = true)
data class Book(
    @Json(name = "id")
    val id: Int,
    @Json(name = "image")
    val image: String,
    @Json(name = "name")
    val name: String,
    @Json(name = "url")
    val url: String
): Parcelable