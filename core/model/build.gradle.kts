plugins {
    id("codechallenge.android.library")
    id("codechallenge.android.moshi")
}

android {
    namespace = "com.saklayen.core.model"

}

dependencies {
    implementation(projects.common.base)
}
