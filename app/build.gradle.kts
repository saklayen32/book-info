plugins {
    id("codechallenge.android.application")
    id("codechallenge.android.hilt")
    id("kotlin-kapt")
    alias(libs.plugins.jetbrainsKotlinAndroid)
}

android {
    namespace = "com.saklayen.bookinfo"
    defaultConfig {
        applicationId = "com.saklayen.codechallenge"
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    packaging {
        resources {
            excludes.addAll(
                arrayOf(
                    "META-INF/io.netty.versions.properties",
                    "META-INF/INDEX.LIST",
                    "**/attach_hotspot_windows.dll",
                    "META-INF/licenses/**",
                    "META-INF/AL2.0",
                    "META-INF/LGPL2.1",
                    "META-INF/gradle/incremental.annotation.processors"
                )
            )
        }
    }

    buildTypes {
        debug {
            isMinifyEnabled = false
        }
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = signingConfigs.getByName("debug")
        }
    }


    buildFeatures {
        buildConfig = true
        dataBinding = true
        viewBinding = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(projects.common.base)
    implementation(projects.common.model)
    implementation(projects.common.ui)
    implementation(projects.core.domain)
    implementation(projects.core.model)
    implementation(projects.core.network)
    implementation(projects.core.data)
    implementation(projects.common.ui)
    implementation(projects.feature.ui)
    implementation(libs.androidx.navigation)
    implementation(libs.androidx.navigation.ui)
}