plugins {
    `kotlin-dsl`
}

group = "com.saklayen.convention"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

dependencies {
    compileOnly(libs.android.gradlePlugin)
    compileOnly(libs.kotlin.gradlePlugin)
}

gradlePlugin {

    plugins {
        register("androidApplication") {
            id = "codechallenge.android.application"
            implementationClass = "AndroidApplicationConventionPlugin"
        }

        register("androidLibrary") {
            id = "codechallenge.android.library"
            implementationClass = "AndroidLibraryConventionPlugin"
        }

        register("androidTest") {
            id = "codechallenge.android.test"
            implementationClass = "AndroidTestConventionPlugin"
        }

        register("androidHilt") {
            id = "codechallenge.android.hilt"
            implementationClass = "AndroidHiltConventionPlugin"
        }

        register("safeArgs") {
            id = "codechallenge.safe.args"
            implementationClass = "SafeArgsConventionPlugin"
        }

        register("androidMoshi") {
            id = "codechallenge.android.moshi"
            implementationClass = "AndroidMoshiConventionPlugin"
        }
    }

}