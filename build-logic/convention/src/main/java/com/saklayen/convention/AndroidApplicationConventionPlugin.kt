/*import com.saklayen.convention.configureFlavors
import com.saklayen.convention.configureKotlinAndroid
import com.saklayen.convention.configurePrintApksTask*/
import com.android.build.api.dsl.ApplicationExtension
import com.saklayen.convention.buildflavor.configureFlavors

import com.saklayen.convention.buildflavor.configureFlavors
import com.saklayen.convention.configureKotlinAndroid
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure

class AndroidApplicationConventionPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("com.android.application")
                apply("org.jetbrains.kotlin.android")
                apply("kotlin-parcelize")
            }

            extensions.configure<ApplicationExtension> {
                configureKotlinAndroid(this)
                defaultConfig.targetSdk = 33
                configureFlavors(this)
            }
        }
    }
}
