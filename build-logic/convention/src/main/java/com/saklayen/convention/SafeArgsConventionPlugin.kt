import org.gradle.api.Plugin
import org.gradle.api.Project

class SafeArgsConventionPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("androidx.navigation:navigation-safe-args-gradle-plugin")
            }
        }
    }
}
