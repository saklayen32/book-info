package com.saklayen.convention.buildflavor

/**
 * This is shared between :app and :benchmarks module to provide configurations type safety.
 */
@Suppress("unused")
enum class CodeChallengeBuildType(val applicationIdSuffix: String? = null) {
    DEBUG,
    RELEASE,
}
