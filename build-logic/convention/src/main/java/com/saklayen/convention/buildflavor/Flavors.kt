@file:Suppress("UnstableApiUsage")

package com.saklayen.convention.buildflavor

import com.android.build.api.dsl.ApplicationExtension
import com.android.build.api.dsl.ApplicationProductFlavor
import com.android.build.api.dsl.CommonExtension
import com.android.build.api.dsl.ProductFlavor
import org.gradle.api.Project

@Suppress("EnumEntryName")
enum class FlavorDimension {
    default
}

@Suppress("EnumEntryName")
enum class CodeChallengeFlavor(
    val dimension: FlavorDimension,
    val applicationIdSuffix: String? = null,
    val versionNameSuffix: String? = null,
    val resource: Resource,
    val buildConfigField: Resource
) {
    dev(
        FlavorDimension.default, ".dev",
        resource = Resource("string", "app_name", "CodeChallenge Dev"),
        buildConfigField = Resource("String", "SERVER_URL", "\"https://example.com/api/\""
        )
    ),
    production(
        FlavorDimension.default,
        resource = Resource("string", "app_name", "CodeChallenge"),
        buildConfigField = Resource("String", "SERVER_URL", "\"https://example.com/api/\""
        )
    )
}

data class Resource(
    val type: String,
    val name: String,
    val value: String
)

fun Project.configureFlavors(
    commonExtension: CommonExtension<*, *, *, *>,
    flavorConfigurationBlock: ProductFlavor.(flavor: CodeChallengeFlavor) -> Unit = {}
) {
    commonExtension.apply {
        flavorDimensions += mutableListOf(FlavorDimension.default.name)
        productFlavors {
            CodeChallengeFlavor.values().forEach {
                create(it.name) {
                    dimension = it.dimension.name
                    flavorConfigurationBlock(this, it)
                    if (this@apply is ApplicationExtension && this is ApplicationProductFlavor) {
                        if (it.applicationIdSuffix != null) {
                            this.applicationIdSuffix = it.applicationIdSuffix
                            this.versionNameSuffix = it.versionNameSuffix
                        }

                        resValue(it.resource.type, it.resource.name, it.resource.value)
                        buildConfigField(it.buildConfigField.type, it.buildConfigField.name, it.buildConfigField.value)
                    }

                }
            }
        }
    }
}
